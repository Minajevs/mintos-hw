package mintos;

import com.netflix.hystrix.exception.HystrixRuntimeException;
import feign.FeignException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeoutException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(FeignException.class)
    public String handleFeignStatusException(FeignException e, HttpServletResponse response) {
        response.setStatus(e.status());
        return "external service unavailable, no existing local record to show";
    }
    @ExceptionHandler(TimeoutException.class)
    public String handleTimeoutException(TimeoutException e, HttpServletResponse response) {
        response.setStatus(500);
        return "external service unavailable, no existing local record to show";
    }

    @ExceptionHandler(HystrixRuntimeException.class)
    public String handleHystrixException(HystrixRuntimeException e, HttpServletResponse response) {
        response.setStatus(500);
        return "external service unavailable, no existing local record to show";
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, HttpServletResponse response) {
        response.setStatus(500);
        return "service unavailable";
    }

}
