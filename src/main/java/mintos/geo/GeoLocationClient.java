package mintos.geo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "location", url = "https://api.ipgeolocationapi.com/", fallback = GeoLocationFallback.class)
public interface GeoLocationClient {

    @RequestMapping(method = RequestMethod.GET, value = "/geolocate/{ip}", produces = "application/json")
    GeoLocationModel getLocation(@PathVariable("ip") String ip);

}
