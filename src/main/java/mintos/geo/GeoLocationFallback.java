package mintos.geo;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GeoLocationFallback implements GeoLocationClient {

    private final GeoLocationRepository geoLocationRepository;
    private final MapperFacade mapperFacade;

    public GeoLocationModel getLocation(String ip) {
        GeoLocation geoLocation = geoLocationRepository.findByIp(ip).orElseThrow(() -> new RuntimeException("Geo IP unavailable, no existing record for IP found: " + ip));
        return mapperFacade.map(geoLocation, GeoLocationModel.class);
    }

}
