package mintos.geo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GeoLocationModel {
    private GeoModel geo;
    private LocalDateTime created;

}
