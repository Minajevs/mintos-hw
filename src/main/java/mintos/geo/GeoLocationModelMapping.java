package mintos.geo;

import ma.glasnost.orika.MapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.stereotype.Component;

@Component
public class GeoLocationModelMapping implements OrikaMapperFactoryConfigurer {

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory.classMap(GeoLocation.class, GeoLocationModel.class)
                .field("latitude", "geo.latitude")
                .field("longitude", "geo.longitude")
                .byDefault()
                .register();
    }

}
