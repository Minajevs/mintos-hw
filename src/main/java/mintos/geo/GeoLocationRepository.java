package mintos.geo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GeoLocationRepository extends JpaRepository<GeoLocation, Long> {
    Optional<GeoLocation> findByIp(String ip);
    GeoLocation findByLatitudeAndLongitude(String latitude, String longitude);
}
