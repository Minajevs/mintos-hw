package mintos.geo;

import lombok.Data;

@Data
public class GeoModel {

    private String latitude;
    private String longitude;

}
