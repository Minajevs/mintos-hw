package mintos.geo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class GeoLocation {

    @Id
    @GeneratedValue
    private Long id;
    private LocalDateTime created;
    private String ip;
    private String latitude;
    private String longitude;
}
