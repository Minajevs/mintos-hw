package mintos.weather;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mintos.geo.GeoLocation;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class Weather {

    @Id
    @GeneratedValue
    private Long id;
    private LocalDateTime created;
    @OneToOne
    private GeoLocation geoLocation;
    private BigDecimal temp;
}
