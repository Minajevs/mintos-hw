package mintos.weather;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class WeatherResponse {

    private LocalDateTime created;
    private BigDecimal temp;


}
