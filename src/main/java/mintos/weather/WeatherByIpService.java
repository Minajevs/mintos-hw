package mintos.weather;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import mintos.geo.GeoLocation;
import mintos.geo.GeoLocationClient;
import mintos.geo.GeoLocationModel;
import mintos.geo.GeoLocationRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
@Slf4j
public class WeatherByIpService {

    private final GeoLocationRepository geoLocationRepository;
    private final WeatherRepository weatherRepository;
    private final GeoLocationClient geoLocationClient;
    private final WeatherClient weatherClient;
    private final MapperFacade mapperFacade;

    @Cacheable("weather")
    public WeatherResponse getWeather(String ip) {
        log.info("not cached");
        GeoLocationModel geoLocationModel = geoLocationClient.getLocation(ip);
        GeoLocation geoLocation = mapperFacade.map(geoLocationModel, GeoLocation.class);
        geoLocation.setIp(ip);
        if(geoLocation.getCreated() == null) {
            geoLocation.setCreated(LocalDateTime.now());
        }
        geoLocationRepository.save(geoLocation);
        WeatherModel weatherModel = weatherClient.getWeather(geoLocationModel.getGeo().getLatitude(), geoLocationModel.getGeo().getLongitude());
        Weather weather = mapperFacade.map(weatherModel, Weather.class);
        weather.setGeoLocation(geoLocation);
        if(weather.getCreated() == null) {
            weather.setCreated(LocalDateTime.now());
        }
        weatherRepository.save(weather);
        return mapperFacade.map(weather, WeatherResponse.class);
    }

}
