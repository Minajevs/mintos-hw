package mintos.weather;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "weather",
        url = "http://api.openweathermap.org/data/2.5",
        fallback = WeatherFallback.class)
public interface WeatherClient {
    @RequestMapping(method = RequestMethod.GET, value = "weather?lat={lat}&lon={lon}&units=metric&appid=71be9e3bc77be083de44c7ee1818bc23", produces = "application/json")
    WeatherModel getWeather(@PathVariable String lat, @PathVariable String lon);

}
