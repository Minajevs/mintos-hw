package mintos.weather;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class WeatherModel {

    private TemperatureModel main;
    private LocalDateTime created;

}
