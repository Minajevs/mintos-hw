package mintos.weather;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/weather")
@AllArgsConstructor
@Slf4j
class WeatherController {

    private final WeatherByIpService weatherByIpService;

    @GetMapping
    ResponseEntity weather(HttpServletRequest request) {
        return ResponseEntity.ok(weatherByIpService.getWeather(getClientIP(request)));
    }

    private String getClientIP(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

}
