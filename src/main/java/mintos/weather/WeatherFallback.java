package mintos.weather;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import mintos.geo.GeoLocation;
import mintos.geo.GeoLocationRepository;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class WeatherFallback implements WeatherClient {

    private final WeatherRepository weatherRepository;
    private final GeoLocationRepository geoLocationRepository;
    private final MapperFacade mapperFacade;

    public WeatherModel getWeather(String lat, String lon) {
        GeoLocation geoLocation = geoLocationRepository.findByLatitudeAndLongitude(lat, lon);
        return mapperFacade.map(weatherRepository.findTopByGeoLocationOrderByCreated(geoLocation).orElseThrow(() -> new RuntimeException("Weather service unavailable, no record for location exists (lat, lon) " + lat + ", " + lon)), WeatherModel.class);
    }

}
