package mintos.weather;

import mintos.geo.GeoLocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WeatherRepository extends JpaRepository<Weather, Long> {

    Optional<Weather> findTopByGeoLocationOrderByCreated(GeoLocation geoLocation);

}
