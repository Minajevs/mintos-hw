package mintos.weather;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TemperatureModel {
    private BigDecimal temp;
}
