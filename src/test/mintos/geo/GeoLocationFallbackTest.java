package mintos.geo;

import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GeoLocationFallbackTest {

    @InjectMocks
    private GeoLocationFallback geoLocationFallback;
    @Mock
    private GeoLocationRepository geoLocationRepository;
    @Mock
    private MapperFacade mapperFacade;
    @Mock
    private GeoLocation geoLocation;
    @Mock
    private GeoLocationModel geoLocationModel;

    @Test
    void getLocation() {

        when(geoLocationRepository.findByIp("123")).thenReturn(Optional.of(geoLocation));
        when(mapperFacade.map(geoLocation, GeoLocationModel.class)).thenReturn(geoLocationModel);
        GeoLocationModel result = geoLocationFallback.getLocation("123");
        assertThat(result).isEqualTo(geoLocationModel);
    }
}