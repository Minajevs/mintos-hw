package mintos.weather;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class WeatherControllerTest {

    @InjectMocks
    private WeatherController weatherController;
    @Mock
    private WeatherByIpService weatherByIpService;
    @Mock
    private HttpServletRequest request;

    @Test
    void weatherForwardedIp() {

        WeatherResponse weatherResponse = new WeatherResponse();
        weatherResponse.setTemp(BigDecimal.valueOf(6.7));
        when(weatherByIpService.getWeather("1234")).thenReturn(weatherResponse);
        when(request.getHeader("X-Forwarded-For")).thenReturn("1234");
        weatherController.weather(request);

        verify(weatherByIpService, times(1)).getWeather("1234");

    }

    @Test
    void weatherRemoteAddr() {

        WeatherResponse weatherResponse = new WeatherResponse();
        when(weatherByIpService.getWeather("9877")).thenReturn(weatherResponse);
        when(request.getRemoteAddr()).thenReturn("9877");
        weatherController.weather(request);
        verify(weatherByIpService, times(1)).getWeather("9877");

    }
}