package mintos.weather;

import ma.glasnost.orika.MapperFacade;
import mintos.geo.GeoLocation;
import mintos.geo.GeoLocationModel;
import mintos.geo.GeoModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MappingTest {

    @Autowired
    MapperFacade mapperFacade;

    @Test
    void testWeatherModelMapper() {

        WeatherModel weatherModel = new WeatherModel();
        TemperatureModel temperature = new TemperatureModel();
        temperature.setTemp(BigDecimal.valueOf(6.7));
        weatherModel.setMain(temperature);
        LocalDateTime now = LocalDateTime.now();
        weatherModel.setCreated(now);
        Weather weather = mapperFacade.map(weatherModel, Weather.class);
        assertThat(weather.getTemp()).isEqualTo(BigDecimal.valueOf(6.7));
        assertThat(weather.getCreated()).isEqualTo(now);
    }
    @Test
    void testGeoLocationModelMapper() {

        GeoLocationModel geoLocationModel = new GeoLocationModel();
        GeoModel geoModel = new GeoModel();
        geoModel.setLatitude("lat1");
        geoModel.setLongitude("long1");
        geoLocationModel.setGeo(geoModel);
        LocalDateTime now = LocalDateTime.now();
        geoLocationModel.setCreated(now);
        GeoLocation geoLocation = mapperFacade.map(geoLocationModel, GeoLocation.class);
        assertThat(geoLocation.getLatitude()).isEqualTo("lat1");
        assertThat(geoLocation.getLongitude()).isEqualTo("long1");
        assertThat(geoLocation.getCreated()).isEqualTo(now);

    }

}