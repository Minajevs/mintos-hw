package mintos.weather;

import ma.glasnost.orika.MapperFacade;
import mintos.geo.GeoLocation;
import mintos.geo.GeoLocationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeatherFallbackTest {

    @InjectMocks
    private WeatherFallback weatherFallback;
    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private GeoLocationRepository geoLocationRepository;
    @Mock
    private MapperFacade mapperFacade;
    @Mock
    private GeoLocation geoLocation;
    @Mock
    private Weather weather;
    @Mock
    private WeatherModel weatherModel;

    @Test
    void getWeather() {

        when(geoLocationRepository.findByLatitudeAndLongitude("lat1", "lon1")).thenReturn(geoLocation);
        when(weatherRepository.findTopByGeoLocationOrderByCreated(geoLocation)).thenReturn(Optional.of(weather));
        when(mapperFacade.map(weather, WeatherModel.class)).thenReturn(weatherModel);
        WeatherModel result = weatherFallback.getWeather("lat1", "lon1");
        assertThat(result).isEqualTo(weatherModel);
    }

}