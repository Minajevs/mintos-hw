package mintos.weather;

import ma.glasnost.orika.MapperFacade;
import mintos.geo.GeoLocation;
import mintos.geo.GeoLocationClient;
import mintos.geo.GeoLocationModel;
import mintos.geo.GeoLocationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeatherByIpServiceTest {

    @InjectMocks
    private WeatherByIpService weatherByIpService;
    @Mock
    private GeoLocationRepository geoLocationRepository;
    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private GeoLocationClient geoLocationClient;
    @Mock
    private WeatherClient weatherClient;
    @Mock
    private MapperFacade mapperFacade;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private GeoLocationModel geoLocationModel;
    @Mock
    private GeoLocation geoLocation;
    @Mock
    private WeatherModel weatherModel;
    @Mock
    private Weather weather;
    @Mock
    private WeatherResponse weatherResponse;

    @Test
    void getWeather() {

        when(geoLocationClient.getLocation("1234")).thenReturn(geoLocationModel);
        when(mapperFacade.map(geoLocationModel, GeoLocation.class)).thenReturn(geoLocation);
        when(geoLocationModel.getGeo().getLatitude()).thenReturn("lat1");
        when(geoLocationModel.getGeo().getLongitude()).thenReturn("long1");
        when(weatherClient.getWeather("lat1", "long1")).thenReturn(weatherModel);
        when(mapperFacade.map(weatherModel,Weather.class)).thenReturn(weather);
        when(mapperFacade.map(weather,WeatherResponse.class)).thenReturn(weatherResponse);
        WeatherResponse result = weatherByIpService.getWeather("1234");
        assertThat(result).isEqualTo(weatherResponse);
        verify(geoLocationRepository).save(geoLocation);
        verify(weatherRepository).save(weather);
    }
}