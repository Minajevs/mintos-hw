# investment platform restful application

To run locally with spring boot :

mvn spring-boot:run
service will be available at localhost:8080
swagger documentation is available at http://localhost:8080/swagger-ui.html
project using H2 embedded database

Assumptions: ip is passed in request header 'X-Forwarded-For', or available from getRemoteAddr. 

## get weather
 
 ```
GET /weather

response example
{
created: "2020-02-03T11:29:25.668",
temp: 2.17
}
created - time when this record was created
temp - temperature in Celcius

```
